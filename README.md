# README

One day we will be able to automatically update the Dewey Decimal codes of arbitrary LibraryThing libraries to match the values from [here](http://classify.oclc.org/classify2/).
However, it is not this day. Before we can do that, we need to look through the [LibraryThing APIs](http://www.librarything.com/services/) and the [Classify APIs](http://classify.oclc.org/classify2/).
Then we need to work out a way to take the ISBNs from LibraryThing, and send them to Classify. Subsequently, we need to take the Dewey codes returned from Classify and send them back to LibraryThing.
The only roadblock is that I don't think LibraryThing has an API for editing book records. Hopefully 'tis but a minor obstacle..
