// ==UserScript==
// @name          LibraryThing ISBN to Dewey
// @namespace     dylandylan1
// @include       *://www.librarything.com/catalog_bottom.php*
// @require       http://code.jquery.com/jquery-1.8.2.min.js
// @version       1
// @grant         GM_xmlhttpRequest
// ==/UserScript==


this.$ = this.jQuery = jQuery.noConflict(true);    //stops jquery interfering with page code


$( document ).ready(function() {    //start doing things when page is loaded

	something_happened = false;
	
	isbn_col = $('#lt_catalog_list #head_isbn').index() + 1;    //determine isbn column
	dewey_col = $('#lt_catalog_list #head_dewey').index() + 1;    //determine dewey column
	
	$rows = $('#lt_catalog_list tbody').children().get();    //get all rows of table body
	
	$('#head_dewey').append('<br/><input id="GMBtn" type="button" value="Start Dewey Fetch" />');    //add button to start dewey fetch
	document.getElementById("GMBtn").addEventListener('click',tableRecursive,true);
	
});


function tableRecursive() {

	$('#GMBtn').attr('disabled','disabled');

	if ($rows.length === 0) {   //stop if no more rows
		if (something_happened) {
			alert('Done! (this page)');
		} else {
			alert('Error! no rows found, nothing happened.');
		}
		return;
	}

	var isbn = $('td:nth-child('+isbn_col+') span', $rows[0]).html();    //get isbn for this row
	
	if (isbn) {
	
		GM_xmlhttpRequest({    //fetch oclc page
			method: "GET",
			url: "http://classify.oclc.org/classify2/ClassifyDemo?search-standnum-txt="+isbn,
			onload: function(response) {
			
				process(response);    // pass response to process()
				
			}
		});
	
	} else {
	
		populate("");    //if no isbn for this row
	
	}

}


function process(response) {

	var $resultObj = $($.parseHTML(response.responseText));    //turn response html into an object
	var new_dewey = $resultObj.find("#classSummaryData > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(2)").html();    //get new dewey value from response
	
	if (new_dewey) {
	
		populate(new_dewey);    // pass new_dewey to populate()

	} else {    //if new dewey value NOT found (when multiple results for given ISBN)
	
		var new_url = $resultObj.find("#results-table > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(1) > span:nth-child(1) > a:nth-child(1)").attr("href");    //get url for first listed (most popular) book for given isbn
		
		if (new_url) {	
		
			GM_xmlhttpRequest({    //fetch new oclc page
				method: "GET",
				url: "http://classify.oclc.org" + new_url,
				onload: function(response) {
				
					process(response);    //process() new response
					
				}
			});
		
		} else {    //if NO result for given ISBN
			
			populate("");    //if oclc doesnt have a dewey for this book
		
		}
		
	}

}


function populate(new_dewey) {

	if (new_dewey) {    //if new dewey value, set it, otherwise skip row

		$('td:nth-child('+dewey_col+')', $rows[0]).dblclick();    //double click dewey box
		$('td:nth-child('+dewey_col+') form textarea', $rows[0]).val(new_dewey);    //set new dewey value
		$('td:nth-child('+dewey_col+') > form > .formButtons > .cancelButton > input:nth-child(1)', $rows[0]).click();    //save new dewey value
	
	}
	
	something_happened = true;	
	$rows.shift();    //remove current row from list of rows to process
	tableRecursive();    //start on next row

}

